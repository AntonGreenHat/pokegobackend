<?php
/*
    Class: LoginModel
    Inherits from : Model
    Purpose : ins and outs for data to the view
    after being processed

    By: Antonio Garcia
    Date : Oct 23 2014
*/
require_once("Model.class.php");
require_once("pokeGOMySQL.class.php");

class LoginModel extends Model
{
    const NO_USER = 0;
    const CEO_USER = 1;
    const ORDERER_USER = 2;
    const SALES_USER = 3;
    protected $myUserName;
    protected $myPassword;//string? or hash?
    protected $myUserType = LoginModel::NO_USER;
    protected $myUserExists;// bool
    protected $myUserID;
    public $username;
    public $password;
    public $mysql;
    public $pokemonName;
    public $pokemonLat;
    public $pokemonLong;
    public $pokemonRadius;
            
    public function process()
    {
        session_start();

    
        $mysql = new pokeGOMySQL();
        
        //right now a post from postman sents a blank for 
        // a username and password to the DB    
            if(isset($_GET['username']) && isset($_GET['password'])) {
                $this->username = $_GET['username'];
                $this->password = $_GET['password'];

                //create user
                // do mysql stuff
                $out = array();
                $mysql_insert_was_successful = true;
                if($mysql_insert_was_successful) {
                    

                    $out['successs'] = true;
                    $this->addPushedUser($mysql);

                } else {
                    $out['success'] = false;
                }
                //echo json_encode($out);
            } else { 
               
            }
        //sends pokemon to DB from IOS GET    
        if(isset($_GET['pokemonName']) && isset($_GET['pokemonLat']) 
        && isset($_GET['pokemonLong']) && isset($_GET['pokemonRadius']))
        {
            $this->pokemonName = $_GET['pokemonName'];
            $this->pokemonLat = $_GET['pokemonLat'];
            $this->pokemonLong = $_GET['pokemonLong'];
            $this->pokemonRadius = $_GET['pokemonRadius'];

            $this->addPokemon($mysql);
        }    

		// check for name being passed in
		if(isset($this->inData['login']))
		{
             //header("location:CreateNewUserController.php");
             $this->myUserName = $this->inData['myUserName'];
             $this->myPassword = $this->inData['myPassword'];

             $_SESSION['myPassword'] = $this->inData['myPassword'];
             $_SESSION['myUserName'] = $this->inData['myUserName'];
         
            $this->outData['myPassword'] = $_SESSION['myPassword'];
            $this->outData['myUserName'] = $_SESSION['myUserName'];

            $this->setGUILocation($mysql);
	
		}
        else if(isset($this->inData['createNewUser']))
        {
            header("location:CreateNewUserController.php");
        }
		else
		{

			// initialize a new session array, this is if the page is refreshed
			
			session_destroy();
			//if the session_destroy does not destroy the session
			//this will force all $_SESSION into a new array
			$_SESSION=array();
		}

    }

// checks if user exists
    protected function checkMyUser($mysql)
    {
        //bool to check if user and pass is correct then it will get the type

        if(!($mysql->checkLogin($this->myUserName, $this->myPassword) == ''))
        {
            $this->myUserID = $mysql->checkLogin($this->myUserName, $this->myPassword);
            $this->myUserExists = true;
            return $this->myUserExists;
        }
        $this->myUserExists = false;
        return $this->myUserExists;
    }

    //gets 1,2 or 3 which is CEO, orderer, sales user respectivly
    //or not user
    protected function getUserTypeNum($mysql)
    {
        $this->checkMyUser($mysql);
        if($this->myUserExists);
        {
            $this->myUserType = LoginModel::USER;
        }
        $this->myUserType = LoginModel::NO_USER;
        return $this->myUserType;

    }

    //sets a header based on the user type during the session
    //while not refressed the session remains 
    protected function setGUILocation($mysql)
    {
        //getting the db user  type you can find out where to send the user
        $this->getUserTypeNum($mysql);
        $myHeaderLocation;
        if($this->myUserType == LoginModel::USER)
        {
            $myHeaderLocation = "UserController.php";
        }
        else if($this->myUserType == LoginModel::NO_USER)
        {
            $myHeaderLocation = "LoginController.php";
        }
        header("location:$myHeaderLocation");
    } 
    //adds user for database
    protected function addPushedUser($mysql)
    {
      
        $mysql->addUser($this->username , $this->password);
        echo "User is added<br>";    
    }    
    //adds Pokemon and properties to AWS DB
    protected function addPokemon($mysql)
    {
        $mysql->addPokemon($this->pokemonName,$this->pokemonLat
            ,$this->pokemonLong,$this->pokemonRadius);
        echo "Pokemon is added";
    }
}
?>
